public class Main {
    public static void main(String[] args) {
        Post post = new Post();
        post.showState();

        post.moveToDraft();
        post.showState();

        post.moveToNotActive();
        post.showState();

        post.publish();
        post.showState();

        //for exception
        post.moveToDraft();
    }
}
