public class PostState {
    private Post post;

    public PostState(Post post) {
        this.post = post;
    }

    public Post getPost() {
        return post;
    }

    public void moveToDraft() {
        throw new IllegalStateException();
    }

    public void moveToNotActive() {
        throw new IllegalStateException();
    }

    public void publish() {
        throw new IllegalStateException();
    }

}
