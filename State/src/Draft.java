public class Draft extends PostState {
    public Draft(Post post) {
        super(post);
    }

    @Override
    public void moveToNotActive() {
        System.out.println(getPost() + " moved to not active");
        getPost().setPostState(new NotActive(getPost()));
    }

    @Override
    public void publish() {
        System.out.println(getPost() + " published");
        getPost().setPostState(new Active(getPost()));
    }
}
