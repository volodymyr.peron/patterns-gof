public class NewPost extends PostState {
    public NewPost(Post post) {
        super(post);
    }

    @Override
    public void moveToDraft() {
        System.out.println(getPost() + " moved to draft");
        getPost().setPostState(new Draft(getPost()));
    }

    @Override
    public void moveToNotActive() {
        System.out.println(getPost() + " moved to not active");
        getPost().setPostState(new NotActive(getPost()));
    }

    @Override
    public void publish() {
        System.out.println(getPost() + " published");
        getPost().setPostState(new Active(getPost()));
    }

}
