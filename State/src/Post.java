public class Post {
    private PostState postState;

    public Post() {
        this.postState = new NewPost(this);
    }

    public void showState() {
        System.out.println(postState.getClass().getName());
    }

    public void setPostState(PostState postState) {
        this.postState = postState;
    }

    public void moveToDraft() {
        postState.moveToDraft();
    }

    public void moveToNotActive() {
        postState.moveToNotActive();
    }

    public void publish() {
        postState.publish();
    }


}
