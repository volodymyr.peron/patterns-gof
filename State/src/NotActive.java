public class NotActive extends PostState {
    public NotActive(Post post) {
        super(post);
    }

    @Override
    public void publish() {
        System.out.println(getPost() + " published");
        getPost().setPostState(new Active(getPost()));
    }
}
