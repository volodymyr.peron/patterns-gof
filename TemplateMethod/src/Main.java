public class Main {
    public static void main(String[] args) {
        Reposter reposter = new FacebookReposter();
        reposter.repostPost();

        System.out.println();

        reposter = new TwitterReposter();
        reposter.repostPost();
    }
}
