public abstract class Reposter {
    abstract void getAccess();

    abstract void createPost();

    abstract void publishPost();

    public final void repostPost() {
        getAccess();
        createPost();
        publishPost();
    }
}
