public class FacebookReposter extends Reposter {
    @Override
    void getAccess() {
        System.out.println("Access to facebook account is provided successfully");
    }

    @Override
    void createPost() {
        System.out.println("Facebook post has been created");
    }

    @Override
    void publishPost() {
        System.out.println("Facebook post has been published");
    }
}
