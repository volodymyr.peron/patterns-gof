public class TwitterReposter extends Reposter {
    @Override
    void getAccess() {
        System.out.println("Access to twitter account is provided successfully");
    }

    @Override
    void createPost() {
        System.out.println("Twitter post has been created");
    }

    @Override
    void publishPost() {
        System.out.println("Twitter post has been published");
    }
}
