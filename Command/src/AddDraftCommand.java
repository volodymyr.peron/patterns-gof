public class AddDraftCommand implements ICommand {
    private PostService receiver;
    private Post draft;

    public AddDraftCommand(PostService receiver, Post draft) {
        this.receiver = receiver;
        this.draft = draft;
    }

    @Override
    public void execute() {
        receiver.addDraft(draft);
    }
}
