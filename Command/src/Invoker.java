import java.util.ArrayList;
import java.util.List;

public class Invoker {
    private List<ICommand> commands = new ArrayList<>();

    public void addCommand(ICommand command) {
        commands.add(command);
    }

    public void executeCommands() {
        commands.forEach(ICommand::execute);
    }
}
