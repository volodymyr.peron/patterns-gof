public class AddArticleCommand implements ICommand {
    private PostService receiver;
    private Post post;

    public AddArticleCommand(PostService receiver, Post post) {
        this.receiver = receiver;
        this.post = post;
    }

    @Override
    public void execute() {
        receiver.addPost(post);
    }
}
