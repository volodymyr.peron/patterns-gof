public class PostService {

    public void addPost(Post post) {
        System.out.println(post + " was added as article");
    }

    public void addDraft(Post post) {
        System.out.println(post + " was added as draft");
    }
}
