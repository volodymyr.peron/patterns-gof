public class Main {
    public static void main(String[] args) {
        PostService postService = new PostService();
        Post post = new Post("title", "content", "post");
        Post draft = new Post("title", "content", "draft");

        ICommand addArticleCommand = new AddArticleCommand(postService, post);
        ICommand addDraftCommand = new AddDraftCommand(postService, draft);

        Invoker invoker = new Invoker();
        invoker.addCommand(addArticleCommand);
        invoker.addCommand(addDraftCommand);

        invoker.executeCommands();
    }
}
