import java.time.LocalDate;

public class Demo {
    public static void main(String[] args) {
        User user = new User.Builder()
                .setName("Name")
                .setEmail("email")
                .setBanned(false)
                .setDateOfBirdth(LocalDate.now())
                .setDateOfLastLogin(LocalDate.now())
                .setPassword("password")
                .build();
        System.out.println(user);
    }
}
