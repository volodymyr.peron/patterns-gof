import java.time.LocalDate;

public class User {
    private String email;
    private String name;
    private String password;
    private LocalDate dateOfBirdth;
    private LocalDate dateOfLastLogin;
    private boolean isBanned;

    private User() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDate getDateOfBirdth() {
        return dateOfBirdth;
    }

    public void setDateOfBirdth(LocalDate dateOfBirdth) {
        this.dateOfBirdth = dateOfBirdth;
    }

    public LocalDate getDateOfLastLogin() {
        return dateOfLastLogin;
    }

    public void setDateOfLastLogin(LocalDate dateOfLastLogin) {
        this.dateOfLastLogin = dateOfLastLogin;
    }

    public boolean isBanned() {
        return isBanned;
    }

    public void setBanned(boolean banned) {
        isBanned = banned;
    }

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", dateOfBirdth=" + dateOfBirdth +
                ", dateOfLastLogin=" + dateOfLastLogin +
                ", isBanned=" + isBanned +
                '}';
    }

    public static class Builder {
        private String email;
        private String name;
        private String password;
        private LocalDate dateOfBirdth;
        private LocalDate dateOfLastLogin;
        private boolean isBanned;

        public Builder setBanned(boolean banned) {
            this.isBanned = banned;
            return this;
        }

        public Builder setDateOfLastLogin(LocalDate dateOfLastLogin) {
            this.dateOfLastLogin = dateOfLastLogin;
            return this;
        }

        public Builder setDateOfBirdth(LocalDate dateOfBirdth) {
            this.dateOfBirdth = dateOfBirdth;
            return this;
        }

        public Builder setPassword(String password) {
            this.password = password;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setEmail(String email) {
            this.email = email;
            return this;
        }

        public User build(){
            User user = new User();
            user.email = email;
            user.name = name;
            user.password = password;
            user.dateOfBirdth = dateOfBirdth;
            user.dateOfLastLogin = dateOfLastLogin;
            user.isBanned = isBanned;
            return user;
        }
    }
}
