import factory.AbstractPostFactory;
import factory.HistoryPostFactory;
import factory.TapePostFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FactoryMethod {
    public static void main(String[] args) {
        List<AbstractPostFactory> factories = new ArrayList<>(Arrays.asList(new HistoryPostFactory(), new TapePostFactory()));

        //createPost() is a factory method
        for (AbstractPostFactory factory : factories) {
            System.out.println(factory.createPost());
        }
    }
}
