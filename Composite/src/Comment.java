import java.util.ArrayList;
import java.util.List;

public class Comment {
    private int articleId;
    private List<Comment> subComments;

    private String content;

    public Comment() {
    }

    public Comment(int articleId, String content) {
        this.articleId = articleId;
        this.content = content;
        this.subComments = new ArrayList<>();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getArticleId() {
        return articleId;
    }

    public void setArticleId(int articleId) {
        this.articleId = articleId;
    }

    public List<Comment> getSubComments() {
        return subComments;
    }

    public void setSubComments(List<Comment> subComments) {
        this.subComments = subComments;
    }

    public void addSubComment(Comment comment) {
        subComments.add(comment);
    }

    public void removeSubComment(Comment comment) {
        subComments.remove(comment);
    }

    public List<String> getAllComments() {
        List<String> comments = new ArrayList<>();
        comments.add(content);
        for (Comment comment : subComments) {
            comments.addAll(comment.getAllComments());
        }
        return comments;
    }
}
