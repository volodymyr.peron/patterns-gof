public class Main {
    public static void main(String[] args) {

        Comment first = new Comment(1, "First Comment");

        Comment a = new Comment(1, "a");
        Comment b = new Comment(1, "b");
        Comment c = new Comment(1, "c");

        Comment a1 = new Comment(1, "a1");
        Comment c1 = new Comment(1, "c1");

        Comment a1_1 = new Comment(1, "a1_1");
        Comment a1_2 = new Comment(1, "a1_2");
        Comment a1_3 = new Comment(1, "a1_3");



        first.addSubComment(a);
        first.addSubComment(b);
        first.addSubComment(c);

        a.addSubComment(a1);
        c.addSubComment(c1);

        a1.addSubComment(a1_1);
        a1.addSubComment(a1_2);
        a1.addSubComment(a1_3);
        first.getAllComments().forEach(System.out::println);


        System.out.println("After removing----------------------------------------------");
        first.removeSubComment(a);
        first.getAllComments().forEach(System.out::println);
    }
}
