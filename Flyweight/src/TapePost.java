public class TapePost extends Post {

    public TapePost(String title, String content) {
        setTitle(title);
        setContent(content);
        setTypeIndicator(PostTypeIndicatorFactory.createTapeIndicator());
    }
}
