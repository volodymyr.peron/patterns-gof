import javax.swing.ImageIcon;
import java.util.HashMap;
import java.util.Map;

public class PostTypeIndicatorFactory {

    private static Map<String, ImageIcon> indicators = new HashMap<>();

    private PostTypeIndicatorFactory() {
        throw new IllegalStateException();
    }

    //TODO: specify real path to images

    public static ImageIcon createHistoryIndicator() {
        if (!indicators.containsKey(HistoryPost.class.getName())) {
            indicators.put(HistoryPost.class.getName(), new ImageIcon("path to history indicator"));
        }
        return indicators.get(HistoryPost.class.getName());
    }

    public static ImageIcon createTapeIndicator() {
        if (!indicators.containsKey(TapePost.class.getName())) {
            indicators.put(TapePost.class.getName(), new ImageIcon("path to history indicator"));
        }
        return indicators.get(TapePost.class.getName());
    }
}
