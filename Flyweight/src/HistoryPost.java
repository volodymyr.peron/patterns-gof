public class HistoryPost extends Post {

    public HistoryPost(String title, String content) {
        setTitle(title);
        setContent(content);
        setTypeIndicator(PostTypeIndicatorFactory.createHistoryIndicator());
    }
}
