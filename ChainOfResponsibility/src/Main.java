public class Main {
    public static void main(String[] args) {
        User user = new User("email", "password", "admin");
        Middleware middleware = new AuthMiddleware();
        middleware.linkWith(new AdminRoleMiddleware());

        PostService postService = new PostService(user, middleware);
        postService.publishAdminPost();

    }
}
