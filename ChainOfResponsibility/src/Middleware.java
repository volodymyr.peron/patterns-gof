public abstract class Middleware {
    private Middleware next;

    public Middleware linkWith(Middleware next) {
        this.next = next;
        return next;
    }

    public boolean check(User user) {
        if (next != null) {
            return next.check(user);
        }
        return true;
    }
}
