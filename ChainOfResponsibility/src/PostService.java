public class PostService {
    private User currentUser;
    private Middleware middleware;

    public PostService(User currentUser, Middleware middleware) {
        this.currentUser = currentUser;
        this.middleware = middleware;
    }

    public void publishAdminPost() {
        if (middleware.check(currentUser)) {
            System.out.println("Admin article was successfully posted!");
        }
    }
}
