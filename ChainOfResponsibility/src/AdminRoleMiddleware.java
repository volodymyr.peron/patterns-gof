public class AdminRoleMiddleware extends Middleware {
    @Override
    public boolean check(User user) {
        if (user.isAdmin()) {
            System.out.println("Is admin");
            return super.check(user);
        }
        return false;
    }
}
