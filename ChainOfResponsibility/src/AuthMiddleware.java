public class AuthMiddleware extends Middleware {
    @Override
    public boolean check(User user) {
        if (user.isAuthorized()) {
            System.out.println("Is authorized");
            return super.check(user);
        }
        return false;
    }
}
