public class ArticleIterator implements Iterator {

    private PostStorage storage;
    private int currentPostIndex;

    public ArticleIterator(PostStorage storage) {
        this.storage = storage;
        currentPostIndex = 0;
    }

    @Override
    public boolean hasNext() {
        return storage.getArticles().size() - 1 >= currentPostIndex;
    }

    @Override
    public Object next() {
        return storage.getArticles().get(currentPostIndex++);
    }
}
