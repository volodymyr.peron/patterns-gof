import java.util.List;
import java.util.stream.Collectors;

public class PostStorage {
    private List<Post> posts;

    public PostStorage() {
    }

    public PostStorage(List<Post> posts) {
        this.posts = posts;
    }

    public void add(Post post) {
        posts.add(post);
    }

    public void addAll(List<Post> newPosts) {
        posts.addAll(newPosts);
    }

    public void remove(Post post) {
        posts.remove(post);
    }

    public void clear() {
        posts.clear();
    }

    public int getSize() {
        return posts.size();
    }

    public Post getPost(int index) {
        return posts.get(index);
    }

    public List<Post> getArticles() {
        return posts.stream()
                .filter(e -> e.getType().equalsIgnoreCase("ARTICLE"))
                .collect(Collectors.toList());
    }

    public List<Post> getDrafts() {
        return posts.stream()
                .filter(e -> e.getType().equalsIgnoreCase("DRAFT"))
                .collect(Collectors.toList());
    }

    public Iterator getArticleIterator() {
        return new ArticleIterator(this);
    }

    public Iterator getDraftIterator() {
        return new DraftIterator(this);
    }

}
