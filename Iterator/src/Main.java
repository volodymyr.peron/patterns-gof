import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Post> posts = new ArrayList<>();
        posts.add(new Post("article1", "content", "article"));
        posts.add(new Post("article2", "content", "article"));
        posts.add(new Post("draft1", "content", "draft"));
        posts.add(new Post("draft2", "content", "draft"));

        PostStorage storage = new PostStorage(posts);

        Iterator articleIterator = storage.getArticleIterator();
        while (articleIterator.hasNext()) {
            System.out.println(articleIterator.next());
        }

        System.out.println("------------------------------------------");

        Iterator draftIterator = storage.getDraftIterator();
        while (draftIterator.hasNext()) {
            System.out.println(draftIterator.next());
        }
    }
}
