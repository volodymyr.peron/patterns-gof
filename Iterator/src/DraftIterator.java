public class DraftIterator implements Iterator {
    private PostStorage storage;
    private int currentPostIndex;

    public DraftIterator(PostStorage storage) {
        this.storage = storage;
        currentPostIndex = 0;
    }

    @Override
    public boolean hasNext() {
        return storage.getDrafts().size() - 1 >= currentPostIndex;
    }

    @Override
    public Object next() {
        return storage.getDrafts().get(currentPostIndex++);
    }
}
