public interface IVisitor {
    void visit(HistoryPost historyPost);

    void visit(TapePost tapePost);

    void visit(User user);
}
