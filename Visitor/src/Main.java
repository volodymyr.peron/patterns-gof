public class Main {
    public static void main(String[] args) {
        User user = new User("", "");
        HistoryPost historyPost = new HistoryPost("title", "content", 0);
        TapePost tapePost = new TapePost("title", "content", 0);

        CommonValidator validator = new CommonValidator();

        user.accept(validator);
        tapePost.accept(validator);
        historyPost.accept(validator);
    }

}
