public class HistoryPost implements IElement {
    private String title;
    private String content;
    private int numberOfViews;

    public HistoryPost(String title, String content, int numberOfViews) {
        this.title = title;
        this.content = content;
        this.numberOfViews = numberOfViews;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getNumberOfViews() {
        return numberOfViews;
    }

    public void setNumberOfViews(int numberOfViews) {
        this.numberOfViews = numberOfViews;
    }

    @Override
    public void accept(IVisitor visitor) {
        visitor.visit(this);
    }
}
