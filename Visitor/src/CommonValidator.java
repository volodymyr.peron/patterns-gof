public class CommonValidator implements IVisitor {
    @Override
    public void visit(HistoryPost historyPost) {
        if (!historyPost.getTitle().equals("") && !historyPost.getContent().equals("") && historyPost.getNumberOfViews() >= 0) {
            System.out.println("history post is valid");
        } else {
            System.out.println("history post is not valid");
        }
    }

    @Override
    public void visit(TapePost tapePost) {
        if (!tapePost.getTitle().equals("") && !tapePost.getContent().equals("") && tapePost.getNumberOfLikes() >= 0) {
            System.out.println("tape post is valid");
        } else {
            System.out.println("tape post is not valid");
        }
    }

    @Override
    public void visit(User user) {
        if (!user.getUsername().equals("") && !user.getPassword().equals("")) {
            System.out.println("user is valid");
        } else {
            System.out.println("user is not valid");
        }
    }
}
