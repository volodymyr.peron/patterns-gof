public class TapePost implements IElement {
    private String title;
    private String content;
    private int numberOfLikes;

    public TapePost(String title, String content, int numberOfLikes) {
        this.title = title;
        this.content = content;
        this.numberOfLikes = numberOfLikes;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getNumberOfLikes() {
        return numberOfLikes;
    }

    public void setNumberOfLikes(int numberOfLikes) {
        this.numberOfLikes = numberOfLikes;
    }

    @Override
    public void accept(IVisitor visitor) {
        visitor.visit(this);
    }
}
