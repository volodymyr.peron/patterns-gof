public class Main {
    public static void main(String[] args) {
        PostCreationContext postCreationContext = new PostCreationContext();

        postCreationContext.mock1();
        postCreationContext.backupPost();

        postCreationContext.mock2();
        postCreationContext.backupPost();

        postCreationContext.mock3();
        postCreationContext.showCurrentState();

        postCreationContext.loadPost();
        postCreationContext.showCurrentState();

        postCreationContext.loadPost();
        postCreationContext.showCurrentState();
    }
}
