import java.util.Deque;
import java.util.LinkedList;

public class PostCreationContext {
    private PostCreator postCreator = new PostCreator();
    private Deque<PostMemento> postsBackups = new LinkedList<>();

    public void mock1() {
        postCreator.fillTitle("mock1 - title");
        postCreator.fillContent("mock1 - content");
        postCreator.fillDescriptionn("mock1 - description");
    }

    public void mock2() {
        postCreator.fillTitle("mock2 - title");
        postCreator.fillContent("mock2 - content");
        postCreator.fillDescriptionn("mock2 - description");
    }

    public void mock3() {
        postCreator.fillTitle("mock3 - title");
        postCreator.fillContent("mock3 - content");
        postCreator.fillDescriptionn("mock3 - description");
        postCreator.fillImagePath("mock3 - image");
    }

    public void backupPost() {
        postsBackups.push(postCreator.backupPost());
    }

    public void loadPost() {
        postCreator.loadPost(postsBackups.pop());
    }

    public void showCurrentState() {
        System.out.println(postCreator.backupPost().getPostState());
    }
}
