public class PostCreator {
    private Post postState;

    public PostCreator() {
        this.postState = new Post();
    }

    public void fillTitle(String title) {
        postState = new Post(title, postState.getContent(), postState.getDescription(), postState.getPathToImage());
    }

    public void fillContent(String content) {
        postState = new Post(postState.getTitle(), content, postState.getDescription(), postState.getPathToImage());
    }

    public void fillDescriptionn(String description) {
        postState = new Post(postState.getTitle(), postState.getContent(), description, postState.getPathToImage());
    }

    public void fillImagePath(String imagePath) {
        postState = new Post(postState.getTitle(), postState.getContent(), postState.getDescription(), imagePath);
    }

    public PostMemento backupPost() {
        return new PostMemento(postState);
    }

    public void loadPost(PostMemento postMemento) {
        postState = postMemento.getPostState();
    }

}
