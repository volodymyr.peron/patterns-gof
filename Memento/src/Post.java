public class Post {
    private String title;
    private String content;
    private String description;
    private String pathToImage;

    public Post() {
    }

    public Post(String title, String content, String description, String pathToImage) {
        this.title = title;
        this.content = content;
        this.description = description;
        this.pathToImage = pathToImage;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getDescription() {
        return description;
    }

    public String getPathToImage() {
        return pathToImage;
    }


    @Override
    public String toString() {
        return "Post{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", description='" + description + '\'' +
                ", pathToImage='" + pathToImage + '\'' +
                '}';
    }
}
