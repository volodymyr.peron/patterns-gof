public class PostMemento {
    private Post postState;

    public PostMemento(Post postState) {
        this.postState = postState;
    }

    public Post getPostState() {
        return postState;
    }
}
