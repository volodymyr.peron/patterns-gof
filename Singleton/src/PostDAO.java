public class PostDAO {

    //TODO: autowire database

    private static volatile PostDAO instance;

    synchronized public static PostDAO getInstance() {
        if(instance == null){
            instance = new PostDAO();
        }
        return instance;
    }

    //TODO: implement methods
    public void createPost() {
        //create post
    }

    public void updatePost() {
        //update post
    }

    public void removePost() {
        //remove post
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
