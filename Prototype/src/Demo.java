public class Demo {
    public static void main(String[] args) {
        UserForPrototype user = new UserForPrototype("email", "name");
        UserForPrototype clone = user.clone();

        clone.setEmail("Updated email");

        if (user != clone) {
            System.out.println(user);
            System.out.println(clone);
        }
    }
}
