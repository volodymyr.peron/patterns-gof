public class UserForPrototype {
    private String email;
    private String name;

    private UserForPrototype(UserForPrototype target) {
        if (target != null) {
            this.email = target.email;
            this.name = target.name;
        }
    }

    public UserForPrototype(String email, String name) {
        this.email = email;
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserForPrototype clone() {
        return new UserForPrototype(this);
    }

    @Override
    public String toString() {
        return "UserForPrototype{" +
                "email='" + email + '\'' +
                ", name='" + name +
                '}';
    }
}
