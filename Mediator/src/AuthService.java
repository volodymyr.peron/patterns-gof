public class AuthService extends Colleague {
    private IMediator mediator;

    public AuthService(IMediator mediator) {
        super(mediator);
    }

    boolean userIsNotBanned(User user) {
        System.out.println(user + "is banned - " + user.isBanned());
        changed();
        return !user.isBanned();
    }
}
