public class NotifyService extends Colleague {
    public NotifyService(IMediator mediator) {
        super(mediator);
    }

    void sendMessage(String message) {
        System.out.println(message + " was sent to email");
        changed();
    }
}
