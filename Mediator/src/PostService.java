public class PostService extends Colleague {
    public PostService(IMediator mediator) {
        super(mediator);
    }

    void addPost(Post post) {
        System.out.println(post + " post was created");
        changed();
    }

    void deletePost(Post post) {
        System.out.println(post + " post was deleted");
        changed();
    }
}
