import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        MainController mainController = new MainController();
        PostService postService = new PostService(mainController);
        AuthService authService = new AuthService(mainController);
        ValidateService validateService = new ValidateService(mainController);

        postService.addPost(new Post("title", "content", "description"));
//        authService.userIsNotBanned(new User.Builder().setBanned(false).build());
//        validateService.validatePost(new Post("sda", "asdas", "dasfdas"));
    }
}
