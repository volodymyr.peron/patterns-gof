public class NavigateService extends Colleague {
    public NavigateService(IMediator mediator) {
        super(mediator);
    }

    public void navigateTo(String window) {
        System.out.println("redirected to " + window);
        changed();
    }
}
