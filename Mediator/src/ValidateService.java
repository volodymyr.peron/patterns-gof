public class ValidateService extends Colleague {
    public ValidateService(IMediator mediator) {
        super(mediator);
    }

    void validatePost(Post post) {
        System.out.println(post + " was validated");
        changed();
    }
}
