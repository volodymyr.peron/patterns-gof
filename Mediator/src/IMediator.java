public interface IMediator {
    void notify(Colleague sender);
}
