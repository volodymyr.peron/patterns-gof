public class MainController implements IMediator {
    private AuthService authService;
    private NotifyService notifyService;
    private ValidateService validateService;
    private PostService postService;
    private NavigateService navigateService;

    public MainController() {
        authService = new AuthService(this);
        notifyService = new NotifyService(this);
        validateService = new ValidateService(this);
        postService = new PostService(this);
        navigateService = new NavigateService(this);
    }

    @Override
    public void notify(Colleague sender) {
        if (sender instanceof AuthService) {
            navigateService.navigateTo("Home page");
        }
        if (sender instanceof PostService) {
            notifyService.sendMessage("article is processed");
            navigateService.navigateTo("Home page");
        }
        if (sender instanceof ValidateService) {
            notifyService.sendMessage("article is validated");
        }
    }
}
