public abstract class Colleague {
    private IMediator mediator;

    public Colleague(IMediator mediator) {
        this.mediator = mediator;
    }

    public void changed() {
        mediator.notify(this);
    }
}
