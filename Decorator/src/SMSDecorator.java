public class SMSDecorator extends NotifierDecorator {

    public SMSDecorator(Notifier wrapper) {
        super(wrapper);
    }

    @Override
    public void send(String message) {
        super.send(message);
        System.out.println(message + " - was sent as SMS");
    }
}
