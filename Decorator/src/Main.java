public class Main {
    private static boolean isFacebookEnabled = true;
    private static boolean isSMSEnabled = true;

    public static void main(String[] args) {
        Notifier baseNotifier = new BaseNotifier();
        if(isSMSEnabled){
            baseNotifier = new SMSDecorator(baseNotifier);
        }
        if(isFacebookEnabled){
            baseNotifier = new FacebookDecorator(baseNotifier);
        }

        baseNotifier.send("message");
    }
}
