public class BaseNotifier implements Notifier {
    @Override
    public void send(String message) {
        System.out.println(message + " - was sent as EMAIL");
    }
}
