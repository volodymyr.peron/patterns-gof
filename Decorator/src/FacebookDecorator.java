public class FacebookDecorator extends NotifierDecorator {

    public FacebookDecorator(Notifier wrapper) {
        super(wrapper);
    }

    @Override
    public void send(String message) {
        super.send(message);
        System.out.println(message + " - was sent as Facebook mail");
    }
}
