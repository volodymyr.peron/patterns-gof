import java.util.ArrayList;
import java.util.List;

public class Post implements ISubject {
    private String content;
    private List<Comment> comments;
    private List<IObserver> observers;

    public Post(String content) {
        this.content = content;
        this.comments = new ArrayList<>();
        this.observers = new ArrayList<>();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public void addComment(Comment comment) {
        comments.add(comment);
        notifyObservers();
    }

    @Override
    public void attachObserver(IObserver observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(IObserver observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        observers.forEach(e -> e.update(this));
    }

    @Override
    public String toString() {
        return "Post{" +
                "content='" + content + '\'' +
                ", comments=" + comments +
                ", observers=" + observers +
                '}';
    }
}
