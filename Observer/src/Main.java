import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        User user1 = new User("user1", "email", LocalDate.now());
        User user2 = new User("user2", "email", LocalDate.now());
        User user3 = new User("user3", "email", LocalDate.now());
        User user4 = new User("user4", "email", LocalDate.now());
        User user5 = new User("user5", "email", LocalDate.now());

        Post post = new Post("default post");
        user1.addPost(post);

        user2.addCommentToPost(new Comment(user2, "comment from user2"), post);
        System.out.println();
        user3.addCommentToPost(new Comment(user3, "comment from user3"), post);
        System.out.println();
        user4.addCommentToPost(new Comment(user4, "comment from user4"), post);
        System.out.println();
        user5.addCommentToPost(new Comment(user5, "comment from user5"), post);

    }
}
