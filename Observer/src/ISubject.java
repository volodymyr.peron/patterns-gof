public interface ISubject {
    void attachObserver(IObserver observer);

    void removeObserver(IObserver observer);

    void notifyObservers();
}
