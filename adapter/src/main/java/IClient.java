public interface IClient {

    void createPost(String postJson);

    void updatePost(String postJson);

    void removePost(int postId);
}
