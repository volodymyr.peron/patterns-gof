import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule());
        JsonToPostAdapter adapter = new JsonToPostAdapter(new PostServiceImpl());

        PostModel post = new PostModel(LocalDate.now(), true, 10);

        String json = mapper.writeValueAsString(post);

        adapter.createPost(json);
    }
}
