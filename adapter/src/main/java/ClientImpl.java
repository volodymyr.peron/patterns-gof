public class ClientImpl implements IClient {


    @Override
    public void createPost(String postJson) {
        System.out.println("Client - " + postJson);
    }

    @Override
    public void updatePost(String postJson) {
        System.out.println("Client - " + postJson);
    }

    @Override
    public void removePost(int postId) {
        System.out.println("Client - " + postId);
    }
}
