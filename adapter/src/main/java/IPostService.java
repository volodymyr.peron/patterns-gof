public interface IPostService {
    void createPost(PostModel post);

    void updatePost(PostModel post);

    void removePost(int postId);
}
