public class PostServiceImpl implements IPostService {
    public void createPost(PostModel post) {
        System.out.println("Service - " + post);
    }

    public void updatePost(PostModel post) {
        System.out.println("Service - " + post);
    }

    public void removePost(int postId) {
        System.out.println("Service - " + postId);
    }
}
