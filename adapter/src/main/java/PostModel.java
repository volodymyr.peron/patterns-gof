import java.time.LocalDate;

public class PostModel{

    private LocalDate date;
    private boolean active;
    private int numberOfViews;

    public PostModel() {
    }

    public PostModel(LocalDate date, boolean active, int numberOfViews) {
        this.date = date;
        this.active = active;
        this.numberOfViews = numberOfViews;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getNumberOfViews() {
        return numberOfViews;
    }

    public void setNumberOfViews(int numberOfViews) {
        this.numberOfViews = numberOfViews;
    }

    @Override
    public String toString() {
        return "PostModel{" +
                "date=" + date +
                ", active=" + active +
                ", numberOfViews=" + numberOfViews +
                '}';
    }
}
