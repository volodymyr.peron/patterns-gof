import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.IOException;

public class JsonToPostAdapter implements IClient {

    private IPostService postService;
    private ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule());

    public JsonToPostAdapter(IPostService postService) {
        this.postService = postService;
    }


    public void createPost(String postJson) {
        try {
            postService.createPost(mapper.readValue(postJson, PostModel.class));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updatePost(String postJson) {
        try {
            postService.updatePost(mapper.readValue(postJson, PostModel.class));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void removePost(int postId) {
        postService.removePost(postId);
    }
}
