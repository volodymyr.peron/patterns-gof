public class TapePostServiceImpl implements IPostService {
    private IPoster poster;

    public TapePostServiceImpl() {
    }

    public TapePostServiceImpl(IPoster poster) {
        this.poster = poster;
    }

    @Override
    public void publishPost() {
        poster.publishPost();
    }
}
