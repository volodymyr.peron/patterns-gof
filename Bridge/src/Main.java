public class Main {
    public static void main(String[] args) {
        IPoster historyPoster = new HistoryPoster();
        IPoster tapePoster = new TapePoster();

        IPostService historyService = new HistoryPostServiceImpl(historyPoster);
        IPostService tapeService = new TapePostServiceImpl(tapePoster);

        historyService.publishPost();
        tapeService.publishPost();
    }
}
