public class HistoryPostServiceImpl implements IPostService {
    private IPoster poster;

    public HistoryPostServiceImpl() {
    }

    public HistoryPostServiceImpl(IPoster poster) {
        this.poster = poster;
    }

    @Override
    public void publishPost() {
        poster.publishPost();
    }
}
