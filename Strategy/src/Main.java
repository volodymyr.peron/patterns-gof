public class Main {
    public static void main(String[] args) {
        AuthService authService = new AuthService();
        authService.authorize();

        authService.setAuthStrategy(new GoogleAuthStrategy());
        authService.authorize();

        authService.setAuthStrategy(new FacebookAuthStrategy());
        authService.authorize();
    }
}
