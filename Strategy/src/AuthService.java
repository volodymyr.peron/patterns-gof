public class AuthService {
    private IAuthStrategy authStrategy = new DefaultAuthStrategy();

    public void setAuthStrategy(IAuthStrategy authStrategy) {
        this.authStrategy = authStrategy;
    }

    public void authorize() {
        authStrategy.authorize();
    }
}
