public class PostService implements IPostService {
    @Override
    public void createPost(PostModel post) {
        System.out.println("Service: creat post");
    }

    @Override
    public void updatePost(PostModel post) {
        System.out.println("Service: update post");
    }

    @Override
    public void removePost(int postId) {
        System.out.println("Service: remove post");
    }
}
