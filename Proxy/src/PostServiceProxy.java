public class PostServiceProxy implements IPostService {

    PostService postService;

    @Override
    public void createPost(PostModel post) {
        checkConnection();
        System.out.println("Proxy: creat post");
        postService.createPost(post);
    }

    @Override
    public void updatePost(PostModel post) {
        checkConnection();
        System.out.println("Proxy: update post");
        postService.updatePost(post);
    }

    @Override
    public void removePost(int postId) {
        checkConnection();
        System.out.println("Proxy: remove post");
        postService.removePost(postId);
    }

    private void checkConnection() {
        if (postService == null) {
            System.out.println("Initialization of post service");
            postService = new PostService();
        }
        //TODO: check connection, if service isn't connected - throw exception
    }
}
