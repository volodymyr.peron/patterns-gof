package factory;

import model.HistoryPost;
import model.Post;

import java.time.LocalDate;

public class HistoryPostFactory implements AbstractPostFactory {
    @Override
    public Post createPost() {
        return new HistoryPost(LocalDate.now(), true, 0);
    }
}
