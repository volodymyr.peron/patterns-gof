package factory;

import model.Post;

public interface AbstractPostFactory {
    Post createPost();
}
