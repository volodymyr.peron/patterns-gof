package factory;

import model.Post;
import model.TapePost;

import java.time.LocalDate;

public class TapePostFactory implements AbstractPostFactory {
    @Override
    public Post createPost() {
        return new TapePost(LocalDate.now(), true, 0);
    }
}
