package model;

import java.time.LocalDate;

public class HistoryPost extends Post {
    private int numberOfViews;

    public HistoryPost() {}

    public HistoryPost(LocalDate date, boolean active, int numberOfViews) {
        this.setDate(date);
        this.setActive(active);
        this.numberOfViews = numberOfViews;
    }

    public int getNumberOfViews() {
        return numberOfViews;
    }

    public void setNumberOfViews(int numberOfViews) {
        this.numberOfViews = numberOfViews;
    }

    public String toString() {
        return "History Post{" +
                "Date of creation = " + getDate() + ", " +
                "Is active = " + isActive() + ", " +
                "Number of views = " + numberOfViews +
                '}';
    }
}
