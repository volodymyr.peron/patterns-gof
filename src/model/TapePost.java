package model;

import java.time.LocalDate;

public class TapePost extends Post {

    private int numberOfLikes;

    public TapePost() {
    }

    public TapePost(LocalDate date, boolean active, int numberOfLikes) {
        this.setDate(date);
        this.setActive(active);
        this.numberOfLikes = numberOfLikes;
    }

    public int getNumberOfLikes() {
        return numberOfLikes;
    }

    public void setNumberOfLikes(int numberOfLikes) {
        this.numberOfLikes = numberOfLikes;
    }

    @Override
    public String toString() {
        return "Tape Post{" +
                "Date of creation = " + getDate() + ", " +
                "Is active = " + isActive() + ", " +
                "Number of likes = " + numberOfLikes +
                '}';
    }

}
