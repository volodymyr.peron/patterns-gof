package model;

import java.time.LocalDate;

public abstract class Post {
    private LocalDate date;
    private boolean active;


    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
