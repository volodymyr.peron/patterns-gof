import factory.AbstractPostFactory;
import model.Post;

import java.util.ArrayList;
import java.util.List;

public class Application {
    private List<Post> posts = new ArrayList<>();
    private AbstractPostFactory factory;

    Application(AbstractPostFactory factory) {
        this.factory = factory;
    }

    public void createPost(){
        posts.add(factory.createPost());
    }

    public void showPosts(){
        posts.forEach(System.out::println);
    }
}
