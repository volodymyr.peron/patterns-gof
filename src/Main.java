import factory.HistoryPostFactory;
import factory.TapePostFactory;

public class Main {


    public static void main(String[] args) {
        User user = new User("Admin", true);
        Application application;
        if (user.isAdministrator()) {
            application = new Application(new TapePostFactory());
        } else {
            application = new Application(new HistoryPostFactory());
        }

        application.createPost();
        application.createPost();
        application.createPost();

        application.showPosts();
    }
}
