public class CommentFacade {
    private CommentService commentService = new CommentService();
    private ValidateService validateService = new ValidateService();
    private Notifier notifier = new BaseNotifier();

    public void addComment(String comment) {
        validateService.validateComment(comment);
        commentService.addComment(comment);
        notifier.send("For your post was attached comment: " + comment);
    }
}
